package fwkLibreria.dto;

public class DetalleLibro extends DetalleEjemplar {

	public enum EstadoLibro {
        NUEVO,
        USADO, // Corresponde a un libro de hasta 2 años de uso
        VIEJO // Corresponde a un libro de mas de 2 años de uso
	}

    private Libro libro;
    private int nuevo,usado,viejo;
    
    public DetalleLibro(Libro libro, int nuevo, int usado, int viejo) {
        this.libro = libro;
        this.nuevo = nuevo;
        this.usado = usado;
        this.viejo = viejo;
    }

    public String getISBN() {
        return libro.getIsbn();
    }

    public String getTitulo() {
    	return libro.getTitulo();
	}

	public Libro getLibro() {
		return libro;
	}

	@Override
	public int getExistencia() {
		return nuevo + usado + viejo;
	}

    @Override
    public String toString() {
        return libro + "; Existencia: (" + nuevo + ") nuevos, (" + usado + ") usados y (" + viejo + ") viejos";
    }


	public int getNuevo() {
		return nuevo;
	}

	public void setNuevo(int nuevo) {
		this.nuevo = nuevo;
	}

	public int getUsado() {
		return usado;
	}

	public void setUsado(int usado) {
		this.usado = usado;
	}

	public int getViejo() {
		return viejo;
	}

	public void setViejo(int viejo) {
		this.viejo = viejo;
	}


	public void restarExistencia(EstadoLibro estadoLibro, int cantidad) {
		switch (estadoLibro) {
			case NUEVO:
				if (cantidad > nuevo) {
					throw new IllegalArgumentException();
				}

				nuevo -= cantidad;
				break;
			case USADO:
				if (cantidad > usado) {
					throw new IllegalArgumentException();
				}

				usado -= cantidad;
				break;
			case VIEJO:
				if (cantidad > viejo) {
					throw new IllegalArgumentException();
				}

				viejo -= cantidad;
				break;
		}

	}
	
	public void sumarExistencia(EstadoLibro estadoLibro, int cantidad) {
		switch (estadoLibro) {
			case NUEVO:
				
				nuevo += cantidad;
				break;
			
			case USADO:
				

				usado += cantidad;
				break;
			case VIEJO:
				

				viejo += cantidad;
				break;
		}

	}

	
	
	
}
