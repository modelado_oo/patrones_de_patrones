package fwkLibreria.dto;

public class Libro extends Ejemplar {
	private String isbn;

	public Libro(String titulo, String autor, String editorial, String isbn) {
        super(titulo, autor, editorial);
        this.isbn = isbn;
    }

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	@Override
	public String toString() {
		return super.toString() + "; ISBN: " + isbn;
	}
}
