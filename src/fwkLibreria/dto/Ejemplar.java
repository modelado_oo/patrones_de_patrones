package fwkLibreria.dto;

public abstract class Ejemplar {
    protected String autor;
    protected String editorial;
    protected String titulo;

    public Ejemplar(String titulo, String autor, String editorial) {
        this.titulo = titulo;
        this.autor = autor;
        this.editorial = editorial;
    }

    public String getAutor() {
        return autor;
    }

    public String getEditorial() {
        return editorial;
    }

    public String getTitulo() {
        return titulo;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " \"" + titulo + "\" escrito por " + autor + " y editado por " + editorial;
    }
}
