package fwkLibreria.iteradores;

import fwkLibreria.dto.Libro;

import java.util.Iterator;
import java.util.List;

public class CatalogoLibrosIterator implements Iterator<Libro> {
    private List<Libro> libros;
    private int posicion = 0;

    public CatalogoLibrosIterator(List<Libro> libros) {
        this.libros = libros;
    }

    @Override
    public boolean hasNext() {
        return posicion < libros.size();
    }

    @Override
    public Libro next() {
        Libro libro = libros.get(posicion);
        posicion++;

        return libro;
    }
}
