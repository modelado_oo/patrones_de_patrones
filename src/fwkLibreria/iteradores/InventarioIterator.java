package fwkLibreria.iteradores;

import fwkLibreria.dto.DetalleLibro;

import java.util.Iterator;
import java.util.List;

public class InventarioIterator implements Iterator<DetalleLibro> {
    private List<DetalleLibro> detalleLibros;
    private int posicion = 0;

    public InventarioIterator(List<DetalleLibro> detalleLibros) {
        this.detalleLibros = detalleLibros;
    }

    @Override
    public boolean hasNext() {
        return posicion < detalleLibros.size();
    }

    @Override
    public DetalleLibro next() {
        DetalleLibro detalleLibro = detalleLibros.get(posicion);
        posicion++;

        return detalleLibro;
    }
}