package fwkLibreria.dao;

import fwkLibreria.Inventario;
import fwkLibreria.dto.Libro;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;

public class DAOResurtido {
    private static HashMap<String, Libro> resurtidos = new HashMap<>();
    private static String RUTA = "resurtidos.txt";
    private TipoGuardado tipoGuardado;

    public DAOResurtido(TipoGuardado tipoGuardado) {
        this.tipoGuardado = tipoGuardado;
    }

    public void solicitarResurtido(Libro libro) {
        resurtidos.putIfAbsent(libro.getIsbn(), libro);

        if (tipoGuardado == TipoGuardado.DISCO) {
            try {
                escribirArchivo();
            } catch (Exception ex) {
                System.err.println(ex.getMessage());
            }
        }
    }

    private void escribirArchivo() throws FileNotFoundException {
        PrintWriter printWriter = new PrintWriter(new FileOutputStream(RUTA));

        resurtidos.values()
                //.forEach(printWriter::println);
                .forEach(libro -> {
                    printWriter.println(libro.getIsbn() + "," + libro.getTitulo() + "," + libro.getEditorial());
                });

        printWriter.close();
    }

    public TipoGuardado getTipoGuardado() {
        return tipoGuardado;
    }

    public void setTipoGuardado(TipoGuardado tipoGuardado) {
        this.tipoGuardado = tipoGuardado;

        if (tipoGuardado == TipoGuardado.DISCO) {
            try {
                escribirArchivo();
            } catch (Exception ex) {
                System.err.println(ex);
            }
        } else {
            try {
                resurtidos.clear();

                Files.lines(Paths.get(RUTA)).forEach(entrada -> {
                    String isbn = entrada.split(",")[0];
                    resurtidos.putIfAbsent(isbn, Inventario.getInstancia().getDetalleLibro(isbn).getLibro());
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public String toString() {
        return "Libros a resurtir: " + resurtidos.values();
    }
}
