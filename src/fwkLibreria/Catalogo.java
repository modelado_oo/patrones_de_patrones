package fwkLibreria;

import java.util.Iterator;

public interface Catalogo<E> {
    Iterator<E> crearIterator();
}
