package fwkLibreria;

import java.util.Iterator;

public interface IInventario <E> {
	
	Iterator<E> crearIterator();

}
