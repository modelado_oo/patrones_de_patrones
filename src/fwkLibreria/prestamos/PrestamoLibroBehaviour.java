package fwkLibreria.prestamos;

import fwkLibreria.Inventario;
import fwkLibreria.dto.DetalleLibro;

public interface PrestamoLibroBehaviour {
    DetalleLibro.EstadoLibro prestar(Inventario inventario, String isbn);
}
