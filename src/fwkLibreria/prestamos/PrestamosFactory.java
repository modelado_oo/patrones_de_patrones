package fwkLibreria.prestamos;

import java.util.Objects;

public class PrestamosFactory {
    public static PrestamoLibroBehaviour crearPrestamoBehaviour(String tipoPrestamo) {
        tipoPrestamo = Objects.requireNonNull(tipoPrestamo).toLowerCase();

        PrestamoLibroBehaviour prestamoLibroBehaviour;

        switch (tipoPrestamo) {
            case "publicogeneral":
                prestamoLibroBehaviour = new PrestamoLibroPublicoGeneral();
                break;
            case "alumno":
                prestamoLibroBehaviour = new PrestamoLibroAlumno();
                break;
            case "profesor":
                prestamoLibroBehaviour = new PrestamoLibroProfesor();
                break;
            default:
                throw new IllegalArgumentException();
        }

        return prestamoLibroBehaviour;
    }
}
