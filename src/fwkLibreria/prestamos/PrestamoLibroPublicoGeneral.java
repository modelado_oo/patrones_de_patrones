package fwkLibreria.prestamos;

import fwkLibreria.Inventario;
import fwkLibreria.dto.DetalleLibro;

import java.util.Objects;

public class PrestamoLibroPublicoGeneral implements PrestamoLibroBehaviour {
    @Override
    public DetalleLibro.EstadoLibro prestar(Inventario inventario, String isbn) {
        Objects.requireNonNull(isbn);

        DetalleLibro.EstadoLibro libroPrestado;

        if (inventario.retirarLibro(isbn, DetalleLibro.EstadoLibro.VIEJO)) {
            libroPrestado = DetalleLibro.EstadoLibro.VIEJO;
        } else if (inventario.retirarLibro(isbn, DetalleLibro.EstadoLibro.USADO)) {
            libroPrestado = DetalleLibro.EstadoLibro.USADO;
        } else if (inventario.retirarLibro(isbn, DetalleLibro.EstadoLibro.NUEVO)) {
            libroPrestado = DetalleLibro.EstadoLibro.NUEVO;
        } else {
            throw new IllegalArgumentException();
        }

        return libroPrestado;
    }
}
