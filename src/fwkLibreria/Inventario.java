package fwkLibreria;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import fwkLibreria.dto.DetalleLibro;
import fwkLibreria.dto.Libro;
import fwkLibreria.iteradores.InventarioIterator;
import observer.Observable;
import observer.Observador;

public class Inventario implements Observable, IInventario<DetalleLibro> {
    //region Observer
    private List<Observador> observadores;

    @Override
    public void registrarObservador(Observador o) {
        observadores.add(o);
    }

    @Override
    public void notificarObservadores() {
        observadores.forEach(observador -> observador.update(ultimoLibroModificado));
    }

    @Override
    public void removerObservador(Observador o) {
        int i = observadores.indexOf(o);

        if (i > 0) {
            observadores.remove(i);
        }
    }
    //endregion

    //region Singleton
    private static Inventario instanciaUnica;

    private Inventario() {
        detalleLibros = new ArrayList<>();
        observadores = new ArrayList<>();
    }


    public static Inventario getInstancia() {
        if(instanciaUnica==null)
            instanciaUnica= new Inventario();
        return instanciaUnica;
    }
    //endregion


	private List<DetalleLibro> detalleLibros;
    private Libro ultimoLibroModificado;
	

	
	@Override
    public Iterator<DetalleLibro> crearIterator() {
        return new InventarioIterator(detalleLibros);
    }

    public void add(DetalleLibro detalleLibro) {
        detalleLibros.add(detalleLibro);
    }

    public void addAll(List<DetalleLibro> detalleLibros) {
        this.detalleLibros.addAll(detalleLibros);
    }

    public void clear() {
        detalleLibros.clear();
    }
    /*
    public List<DetalleLibro> get(String isbn) {
        return detalleLibros.stream()
                .filter(libro -> libro.getISBN().compareTo(isbn)==0)
                .collect(Collectors.toList());
    }
    */
    
    public DetalleLibro getDetalleLibro(String isbn) {
        Objects.requireNonNull(isbn);
    	DetalleLibro detalleLibroEncontrado = null;
    	
    	for(DetalleLibro detalleLibro : detalleLibros) {
    		
    		if(detalleLibro.getISBN().compareTo(isbn)==0) {
                detalleLibroEncontrado = detalleLibro;
                break;
            }
    	}

        Objects.requireNonNull(detalleLibroEncontrado);

    	return detalleLibroEncontrado;
    }

    public boolean retirarLibro(String isbn, DetalleLibro.EstadoLibro estadoLibro) {
        DetalleLibro detalleLibro = getDetalleLibro(isbn);

        boolean retirado = false;

        switch (estadoLibro) {
            case NUEVO:
                if (detalleLibro.getNuevo() > 0) {
                    detalleLibro.restarExistencia(DetalleLibro.EstadoLibro.NUEVO, 1);
                    retirado = true;
                }
                break;
            case USADO:
                if (detalleLibro.getUsado() > 0) {
                    detalleLibro.restarExistencia(DetalleLibro.EstadoLibro.USADO, 1);
                    retirado = true;
                }
                break;
            case VIEJO:
                if (detalleLibro.getViejo() > 0) {
                    detalleLibro.restarExistencia(DetalleLibro.EstadoLibro.VIEJO,1);
                    retirado = true;
                }
                break;
        }

        ultimoLibroModificado = detalleLibro.getLibro();

        if (detalleLibro.getExistencia() < 2) {
            notificarObservadores();
        }

        return retirado;
    }
    
    public void devolverLibro(String isbn, DetalleLibro.EstadoLibro estadoLibro) {
        DetalleLibro detalleLibro = getDetalleLibro(isbn);

        switch (estadoLibro) {
            case NUEVO:
               
                 detalleLibro.sumarExistencia(DetalleLibro.EstadoLibro.NUEVO, 1);
                    
                 break;
            case USADO:
                
                detalleLibro.sumarExistencia(DetalleLibro.EstadoLibro.USADO, 1);
                break;
            
            case VIEJO:
                
                detalleLibro.sumarExistencia(DetalleLibro.EstadoLibro.VIEJO,1);
                break;
        }

        ultimoLibroModificado = detalleLibro.getLibro();
        
    }
}
