package fwkLibreria;

import fwkLibreria.dto.Libro;
import fwkLibreria.iteradores.CatalogoLibrosIterator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class CatalogoLibros implements Catalogo<Libro> {
	private List<Libro> libros;

//region Singleton
    private static CatalogoLibros instanciaUnica;

    private CatalogoLibros() {
        libros = new ArrayList<>();
    }
    
    
    public static CatalogoLibros getInstancia() {
        if(instanciaUnica==null)
        	instanciaUnica= new CatalogoLibros();
    	return instanciaUnica;
    }
//endregion

    @Override
    public Iterator<Libro> crearIterator() {
        return new CatalogoLibrosIterator(libros);
    }

    public void add(Libro libro) {
        libros.add(libro);
    }

    public void addAll(List<Libro> libros) {
        this.libros.addAll(libros);
    }

    public void clear() {
        libros.clear();
    }

    public List<Libro> get(String isbn) {
        return libros.stream()
                .filter(libro -> libro.getIsbn().compareTo(isbn)==0)
                .collect(Collectors.toList());
    }
    
    public Libro getLibro(String isbn) {
        Objects.requireNonNull(isbn);
    	Libro libroEncontrado= null;
    	
    	for(Libro libro: libros) {
    		
    		if(libro.getIsbn().compareTo(isbn)==0)
    			libroEncontrado=libro;
    		
    	}

        Objects.requireNonNull(libroEncontrado);
    	
    	return libroEncontrado;
    }
    
}
