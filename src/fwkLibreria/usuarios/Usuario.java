package fwkLibreria.usuarios;

import biblioCITECApp.BiblioCITECApp;
import fwkLibreria.dto.DetalleLibro;

public abstract class Usuario {
    protected String appMaterno;
    protected String appPaterno;
    protected String clave;
    protected String nombre;
    

    protected BiblioCITECApp sistema;

    public Usuario (BiblioCITECApp sistema) {
        this.sistema = sistema;
    }

    public void pedirPrestamo(String isbn) {
        sistema.pedirPrestamo(this, isbn);
    }
    
    public void devolverLibro(String isbn, DetalleLibro.EstadoLibro estadoLibro) {
        sistema.devolverLibro(isbn,estadoLibro);
    }

	public String getAppMaterno() {
		return appMaterno;
	}

	public void setAppMaterno(String appMaterno) {
		this.appMaterno = appMaterno;
	}

	public String getAppPaterno() {
		return appPaterno;
	}

	public void setAppPaterno(String appPaterno) {
		this.appPaterno = appPaterno;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public BiblioCITECApp getSistema() {
		return sistema;
	}

	public void setSistema(BiblioCITECApp sistema) {
		this.sistema = sistema;
	}
    
}
