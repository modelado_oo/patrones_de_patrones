package fwkLibreria.usuarios;

import biblioCITECApp.BiblioCITECApp;

public abstract class UsuarioFactory {
    protected abstract Usuario crearUsuario(BiblioCITECApp sistema, String tipo);

    public Usuario generarUsuario(BiblioCITECApp sistema, String tipo) {
        Usuario usuario = crearUsuario(sistema, tipo);

        // Cosas que hacer al usuario.

        return usuario;
    }
}
