package fwkLibreria.usuarios;

import biblioCITECApp.BiblioCITECApp;

import java.util.Objects;

public class StandardUsuarioFactory extends UsuarioFactory {

    @Override
    protected Usuario crearUsuario(BiblioCITECApp sistema, String tipo) {
        tipo = Objects.requireNonNull(tipo).toLowerCase();

        Usuario usuario;

        switch (tipo) {
            case "publicogeneral":
                usuario = new PublicoGeneral(sistema);
                break;
            case "profesor":
                usuario = new Profesor(sistema);
                break;
            case "alumno":
                usuario = new Alumno(sistema);
                break;
            default:
                throw new IllegalArgumentException();
        }

        return usuario;
    }

}
