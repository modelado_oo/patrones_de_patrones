package observer;

import fwkLibreria.dto.Libro;

public interface Observador {
    void update(Libro libro);
}
