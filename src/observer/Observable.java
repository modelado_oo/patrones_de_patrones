package observer;

public interface Observable {
    void registrarObservador(Observador observer);
    void notificarObservadores();
    void removerObservador(Observador observador);
}
