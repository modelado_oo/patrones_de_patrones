package tests;

import fwkLibreria.dto.Libro;

import java.util.ArrayList;
import java.util.List;

public class BrutalTests {
    public static void main (String[] args) {
        List<Libro> libros = new ArrayList<>();
        libros.add(new Libro("Head First Design Patterns", "Eric Freeman & Elisabeth Robson",
                "O'Reilly", "978-0-596-00712-6"));

        System.out.println(libros);
    }
}
