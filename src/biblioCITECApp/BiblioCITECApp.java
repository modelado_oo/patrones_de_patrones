package biblioCITECApp;

import fwkLibreria.Catalogo;
import fwkLibreria.CatalogoLibros;
import fwkLibreria.Inventario;
import fwkLibreria.dao.DAOResurtido;
import fwkLibreria.dao.TipoGuardado;
import fwkLibreria.dto.Libro;
import fwkLibreria.dto.DetalleLibro;
import fwkLibreria.prestamos.PrestamoLibroBehaviour;
import fwkLibreria.prestamos.PrestamosFactory;
import fwkLibreria.usuarios.*;
import observer.Observador;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class BiblioCITECApp implements Observador {
	private CatalogoLibros catalogo;
	private Inventario inventario;

    private enum ComandoApp {
        INICIALIZAR_BD,
        MOSTRAR_BD,
        PRESTAR,
        DEVOLVER,
        CAMBIAR_USUARIO,
        MOSTRAR_RESURTIDO,
        CONFIGURAR,
        SALIR;

        public static ComandoApp get(int num) {
            if (num < 1 || num > values().length) {
                throw new IndexOutOfBoundsException("No existe un comando con valor " + num);
            }

            return values()[num - 1];
        }
    }

    private ComandoApp comando;
    private Scanner scanner;
    private DAOResurtido daoResurtido;
    //private Usuario usuarioActual;
    private TipoGuardado tipoGuardado;
    private Usuario usuarioActual= new Alumno(this);
    private StandardUsuarioFactory factoryUsuarios = new StandardUsuarioFactory();

    private BiblioCITECApp() {
        comando = ComandoApp.INICIALIZAR_BD;
        scanner = new Scanner(System.in);

        usuarioActual = new Alumno(this); // alumno
        tipoGuardado = TipoGuardado.MEMORIA;
        catalogo = CatalogoLibros.getInstancia();
        inventario = Inventario.getInstancia();
        inventario.registrarObservador(this);
        daoResurtido = new DAOResurtido(tipoGuardado);
        /*
         1. Inicializar el sistema con base de datos ficticia
         2. Mostrar la base de datos
         3. Prestar libros
         4. Devolver libros
         5. Cambiar tipo de usuario
         6. Mostrar titulos a resurtir
         7. Configurar sistema ("titulos a surtir" Memoria/Disco)
         8. Salir
        */

        inicializaBD();
        mostrarBD();

         do {
             try {
                 switch (comando) {
                     case INICIALIZAR_BD:
                         inicializaBD();
                         break;
                     case MOSTRAR_BD:
                         mostrarBD();
                         break;
                     case PRESTAR:
                         prestarGUI();
                         break;
                     case DEVOLVER:
                    	 devolverGUI();
                         break;
                     case CAMBIAR_USUARIO:
                    	 cambiarUsuarioGUI();
                                            	 
                         break;
                     case MOSTRAR_RESURTIDO:
                         println(daoResurtido);
                         break;
                     case CONFIGURAR:
                         if (tipoGuardado == TipoGuardado.MEMORIA)
                             tipoGuardado = TipoGuardado.DISCO;
                         else
                             tipoGuardado = TipoGuardado.MEMORIA;

                         println("Sistema configurado para guardar resurtidos en " + tipoGuardado);
                         daoResurtido.setTipoGuardado(tipoGuardado);
                         break;
                 }
             } catch (Exception ex) {
                 System.err.println(ex.getMessage());
             }

             println("<< Menu prueba BiblioCITEC>> \n");
             println("1. Inicializar BD");
             println("2. Mostrar BD");
             println("3. Prestar");
             println("4. Devolver");
             println("5. Cambiar Usuario");
             println("6. Mostrar Resurtido");
             println("7. Configurar");
             println("8. Salir\n");

             comando = solicitarOpcionUsuario();
            	 
         } while (comando != ComandoApp.SALIR);
    }

    private ComandoApp solicitarOpcionUsuario() {
        System.out.print("(" + usuarioActual.getClass().getSimpleName()+ ") Elija una opcion: ");
        String numero = scanner.nextLine();

        while (!numero.matches("[0-9]")) {
            println("Opcion incorrecta, intente denuevo.");
            System.out.print("(" + usuarioActual.getClass().getSimpleName()+ ") Elija una opcion: ");
            numero = scanner.nextLine();
        }

        return ComandoApp.get(Integer.parseInt(numero));
    }

    private void inicializaBD() {


        Libro libro1 = new Libro("Pedro Paramos","Juan Rulfo",
                "Fondo de Cultura Economica","0001");
        Libro libro2 = new Libro("Tus Zonas Erroneas","Wayne W. Dyner",
                "Avon Books","0002");
        Libro libro3 = new Libro("Budismo Zen y el psicoanalisis ","Suzuki y Erich Fromm",
                "Fondo de Cultura Economica","0003");
        Libro libro4 = new Libro("Head First Design Patterns", "Eric Freeman & Elisabeth Robson",
                "O'Reilly", "0004");

        List<Libro> libros = new ArrayList<>();
        libros.add(libro1);
        libros.add(libro2);
        libros.add(libro3);

        DetalleLibro exDetalleLibro1 = new DetalleLibro(libro1,1,2,3);
        DetalleLibro exDetalleLibro2 = new DetalleLibro(libro2,3,2,1);
        DetalleLibro exDetalleLibro3 = new DetalleLibro(libro3,4,5,7);
        DetalleLibro exDetalleLibro4 = new DetalleLibro(libro4, 1, 1, 1);

        List<DetalleLibro> invDetalleLibros = new ArrayList<>();
        invDetalleLibros.add(exDetalleLibro1);
        invDetalleLibros.add(exDetalleLibro2);
        invDetalleLibros.add(exDetalleLibro3);
        invDetalleLibros.add(exDetalleLibro4);

        catalogo.clear();
        catalogo.addAll(libros);
        inventario.clear();
        inventario.addAll(invDetalleLibros);

        println("Base de datos inicializada.");
    }

    private void mostrarBD() {

        Iterator<Libro> iteradorLibros = catalogo.crearIterator();

        while (iteradorLibros.hasNext()){

            Libro libro = iteradorLibros.next();
            DetalleLibro detalleLibroExistencia = inventario.getDetalleLibro(libro.getIsbn());

            System.out.println("ISBN: " + libro.getIsbn());
            System.out.println("Titulo: " + libro.getTitulo());
            System.out.println("Autor: " + libro.getAutor());
            System.out.println("Editorial: " + libro.getEditorial());
            System.out.println("Existencia: ");
            System.out.println("Nuevos: " + detalleLibroExistencia.getNuevo());
            System.out.println("Usados: " + detalleLibroExistencia.getUsado());
            System.out.println("Viejos: " + detalleLibroExistencia.getViejo());
            System.out.println("");

        }
    }

    private void prestarGUI() {
        System.out.print("Escriba el ISBN del libro: ");
        String isbn = scanner.nextLine();
        DetalleLibro detalleLibro = inventario.getDetalleLibro(isbn);
        println("Libro a prestar: " + detalleLibro.getTitulo());
        DetalleLibro.EstadoLibro libroPrestado = pedirPrestamo(usuarioActual, isbn);
        println("Se presto un libro " + libroPrestado);
    }
    
    private void devolverGUI() {
    	String estado="";
    	String isbn="";
    	
    	do {
    		
    	
    	System.out.print("Escriba el ISBN del libro: ");
        isbn = scanner.nextLine();
        DetalleLibro detalleLibro = inventario.getDetalleLibro(isbn);
        println("Libro devuelto: " + detalleLibro.getTitulo());
        println("Seleccione el estado del libro: ");
        println("1.- NUEVO, 2.- USADO, 3.- VIEJO");
        estado = scanner.nextLine();
        
        switch (estado) {
        	case "1":
        		devolverLibro(isbn,DetalleLibro.EstadoLibro.NUEVO);
        		break;
        	case "2":
        		devolverLibro(isbn,DetalleLibro.EstadoLibro.USADO);
        		break;
        	case "3":
        		devolverLibro(isbn,DetalleLibro.EstadoLibro.VIEJO);
        		break;
        		
        	default:
        		println("Opcion invalida");
        		break;
        
        }
        
    	}while( (estado.compareTo("1")!=0) && (estado.compareTo("2")!=0) && (estado.compareTo("3")!=0));
        
        println("Libro Devuelto exitosamente ");
    }
    
    private void cambiarUsuarioGUI() {
    	String estado="";
    	
    	    	
    	do {
    	
    	System.out.println("Escriba el Tipo de Usuario al que desea cambiar: ");
        println("Tipo de usuario: ");
        println("1.- Alumno, 2.- Profesor, 3.- Publico General");
        estado = scanner.nextLine();
        
        switch (estado) {
        	case "1":
        		usuarioActual=cambiarUsuario(usuarioActual,"alumno",factoryUsuarios);
        		break;
        	case "2":
                usuarioActual=cambiarUsuario(usuarioActual,"profesor",factoryUsuarios);
        		break;
        	case "3":
                usuarioActual=cambiarUsuario(usuarioActual,"publicogeneral",factoryUsuarios);
        		break;
        		
        	default:
        		println("Opcion invalida");
        		break;
        
        }
        
    	}while( (estado.compareTo("1")!=0) && (estado.compareTo("2")!=0) && (estado.compareTo("3")!=0));
        
        
    }


    public DetalleLibro.EstadoLibro pedirPrestamo(Usuario usuario, String isbn) {
        PrestamoLibroBehaviour prestamoLibroBehaviour = PrestamosFactory.crearPrestamoBehaviour(usuario.getClass().getSimpleName());

        return prestamoLibroBehaviour.prestar(inventario, isbn);
    }
    
    public void devolverLibro(String isbn, DetalleLibro.EstadoLibro estadoLibro) {
        
    	inventario.devolverLibro(isbn, estadoLibro);
    
    }

    private void solicitarResurtido(Libro libro) {
        daoResurtido.solicitarResurtido(libro);
    }

    @Override
    public void update(Libro libro) {
        System.out.println("Solicitando resurtido de " + libro);
        daoResurtido.solicitarResurtido(libro);
    }

public Usuario cambiarUsuario(Usuario usuario, String tipoUsuario, StandardUsuarioFactory factory) {
    	
    	String clave = usuario.getClave();
    	String apePaterno = usuario.getAppPaterno();
    	String apeMaterno = usuario.getAppMaterno();
    	String nombre = usuario.getNombre();
    	
    	
    	if(usuario instanceof Alumno) {
    		
    		if( (tipoUsuario.compareTo("profesor")==0) || (tipoUsuario.compareTo("publicogeneral")==0)) {
    			usuario = factory.generarUsuario(this, tipoUsuario);
    			println("Usuario cambiado exitosamente ");
    			println("Tipo de usuario cambiado a " + usuario.getClass().getSimpleName() + ".");
    			return usuario;
    		}else {
    			
    			println("Un Alumno solo puede cambiar a Profesor o Publico General");
    		}
    	
    	}
    	
    	if(usuario instanceof Profesor) {
    		
    		if( tipoUsuario.compareTo("publicogeneral")==0) {
    			usuario = factory.generarUsuario(this, tipoUsuario);
    			println("Usuario cambiado exitosamente ");
    			println("Tipo de usuario cambiado a " + usuario.getClass().getSimpleName() + ".");
    			return usuario;
    		}else {
    			
    			println("Un Profesor solo puede cambiar a Publico General");
    		}
    	
    	}
    	
    	if(usuario instanceof PublicoGeneral) {
    		
    		if( (tipoUsuario.compareTo("profesor")==0) || (tipoUsuario.compareTo("alumno")==0)) {
    			usuario = factory.generarUsuario(this, tipoUsuario);
    			println("Usuario cambiado exitosamente ");
    			println("Tipo de usuario cambiado a " + usuario.getClass().getSimpleName() + ".");
    			return usuario;
    		}else {
    			
    			println("Publico General solo puede cambiar a Alumno o Profesor");
    		}
    	
    	}
    	
    	usuario.setClave(clave);
    	usuario.setNombre(nombre);
    	usuario.setAppPaterno(apePaterno);
    	usuario.setAppMaterno(apeMaterno);
    	
    	
    	return usuario;
    	
    }
    

    //region Main
    public static void main(String[] args) {
        //LibrosDAO daoResurtido = new LibrosDAO(CatalogoLibros.getInstancia());
        new BiblioCITECApp();
    }

    private static void println(Object o) {
        System.out.println(o);
    }
    //endregion

    
}
