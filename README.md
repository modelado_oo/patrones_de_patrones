Implementación de un sistema en java encargado del control de préstamos y devoluciones de un catálogo de libros
utilizando los siguientes patrones de diseño:

    - Strategy
    - Observer
    - Singleton
    - Decorator
    - Iterator
    - Adapter
    - Factory Method
    - Template Method